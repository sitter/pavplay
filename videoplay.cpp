#include <QApplication>
#include <QStringList>

#include <phonon/MediaObject>
#include <phonon/VideoWidget>
#include <phonon/MediaSource>

using namespace Phonon;

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("videplay");
    MediaObject mo;
    mo.setCurrentSource(MediaSource(app.arguments().at(1)));
    VideoWidget vw;
    createPath(&mo, &vw);
    mo.play();
    vw.show();
    return app.exec();
}
