#include <QCoreApplication>
#include <QStringList>

#include <phonon/MediaObject>
#include <phonon/AudioOutput>
#include <phonon/MediaSource>

using namespace Phonon;

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    app.setApplicationName("audioplay");
    MediaObject mo;
    mo.setCurrentSource(MediaSource(app.arguments().at(1)));
    AudioOutput ao;
    createPath(&mo, &ao);
    mo.play();
    return app.exec();
}
